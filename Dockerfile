FROM pihole/pihole:latest
ENV GS_INSTALL primary
ENV DEBIAN_FRONTEND noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update && apt-get install --no-install-recommends openssh-server openssh-client rsync git -y && /etc/init.d/ssh start && apt-get clean
RUN curl -sSL https://gravity.vmstan.com | bash
RUN update-rc.d ssh defaults
EXPOSE 53/tcp
EXPOSE 53/udp
EXPOSE 67/udp
EXPOSE 80/tcp
EXPOSE 22/tcp
HEALTHCHECK CMD dig +short +norecurse +retry=0 @127.0.0.1 pi.hole || exit 1

SHELL ["/bin/bash", "-c"]